package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"path/filepath"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type login struct {
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}
type Admins struct {
	ID       string
	Name     string `form:"Name" json:"Name"`
	Email    string `form:"Email" gorm:"unique" json:"Email"`
	Phone    string `form:"Phone" json:"Phone"`
	Address  string `form:"Address" json:"Address"`
	Password string `form:"Password" json:"Password" binding:"required"`
	Photo    string `form:"Photo" json:"Photo"`
	Status   string `form:"Status" json:"Status"`
}

var identityKey = "id"

func sendEmail(email []string) {
	from := "muhammadjawahiruzzaman@gmail.com"
	password := "yusup999"

	// Receiver email address.
	to := email
	// smtp server configuration.
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// Message.
	message := []byte("This is a test email message.")

	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Sending email.
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Email Sent Successfully!")
}
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// User demo
type User struct {
	UserName  string
	FirstName string
	LastName  string
}

func main() {
	port := os.Getenv("PORT")
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	r.MaxMultipartMemory = 8 << 20 // 8 MiB

	if port == "" {
		port = "8000"
	}
	// same as
	// config := cors.DefaultConfig()
	// config.AllowAllOrigins = true
	// r.Use(cors.New(config))
	r.Use(cors.Default())

	dsn := "root:@tcp(localhost:3306)/explore?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("tidak dapat terhubung dengan db")
	}

	// the jwt middleware
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "test zone",
		Key:         []byte("secret key"),
		Timeout:     time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*User); ok {
				return jwt.MapClaims{
					identityKey: v.UserName,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &User{
				UserName: claims[identityKey].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			userID := loginVals.Email
			password := loginVals.Password

			var admin Admins
			db.Where("email = @email", sql.Named("email", userID)).Find(&admin)

			if userID == admin.Email && CheckPasswordHash(password, admin.Password) {
				return &User{
					UserName:  userID,
					LastName:  "Bo-Yi",
					FirstName: "Wu",
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		//Authorizator: func(data interface{}, c *gin.Context) bool {
		//	if v, ok := data.(*User); ok && v.UserName == "admin" {
		//		return true
		//	}
		//
		//	return false
		//},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		SendCookie:     true,
		SecureCookie:   false, //non HTTPS dev environments
		CookieHTTPOnly: true,  // JS can't modify
		CookieDomain:   "localhost",
		CookieName:     "token", // default jwt
		TokenLookup:    "cookie:token",
		CookieSameSite: http.SameSiteDefaultMode,
		TokenHeadName:  "Bearer",
		TimeFunc:       time.Now,
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}

	// When you use jwt.New(), the function is already automatically called for checking,
	// which means you don't need to call it again.
	errInit := authMiddleware.MiddlewareInit()

	if errInit != nil {
		log.Fatal("authMiddleware.MiddlewareInit() Error:" + errInit.Error())
	}

	r.POST("/login", authMiddleware.LoginHandler)

	r.NoRoute(authMiddleware.MiddlewareFunc(), func(c *gin.Context) {
		claims := jwt.ExtractClaims(c)
		log.Printf("NoRoute claims: %#v\n", claims)
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})

	auth := r.Group("/auth")
	// Refresh time can be longer than token timeout
	auth.GET("/refresh_token", authMiddleware.RefreshHandler)
	auth.Use(authMiddleware.MiddlewareFunc())
	{

		auth.POST("/create-admin", func(c *gin.Context) {
			var input Admins
			if err := c.ShouldBindJSON(&input); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}

			admin := Admins{
				Name:    input.Name,
				Email:   input.Email,
				Phone:   input.Phone,
				Address: input.Address,
			}

			db.Create(&admin)

			email := []string{
				input.Email,
			}
			sendEmail(email)

			c.JSON(http.StatusOK, gin.H{"data": admin, "message": "Verifikasi email anda telah dikirim"})
		})
		auth.GET("/get-admin", func(c *gin.Context) {

			var admin []Admins

			db.Find(&admin)

			c.JSON(200, admin)
		})
		// jika upload foto di endpoint maka akan keluar error eof
		auth.PUT("/activation-account/:id", func(c *gin.Context) {
			id := c.Param("id")
			var input Admins
			var model []Admins

			// Upload the file to specific dst.
			db.Find(&model)

			if err := c.ShouldBindJSON(&input); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}

			for i, a := range model {
				if a.ID == id {
					Password, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
					model[i].Password = string(Password)
					model[i].Status = "active"

					db.Save(&model)

					c.JSON(http.StatusOK, gin.H{
						"error":   false,
						"message": "success",
					})
					return
				}
			}
			c.JSON(http.StatusNotFound, gin.H{
				"error":   true,
				"message": "invalid admin id ",
			})
		})

		r.POST("/upload", func(c *gin.Context) {
			file, err := c.FormFile("file")
			if err != nil {
				c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
				return
			}

			filename := "images/" + fmt.Sprint(time.Now().UnixNano()) + filepath.Ext(file.Filename)
			if err := c.SaveUploadedFile(file, filename); err != nil {
				c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
				return
			}

			c.String(http.StatusOK, fmt.Sprintf("File %s uploaded successfully", file.Filename))
		})

	}

	if err := http.ListenAndServe(":"+port, r); err != nil {
		log.Fatal(err)
	}

}
